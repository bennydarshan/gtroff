#include<iostream>
#include<fstream>
#include<thread>
#include<chrono>
#include <cstdio>
#include <memory>
#include <stdexcept>
#include <string>
#include <array>

#include<gtkmm.h>
#include<evince-document.h>
#include<evince-view.h>

EvDocument *doc;
Glib::RefPtr<Gtk::TextBuffer> pbuff;
int running = 1;
EvDocumentModel *model;


void tview_changed()
{
    //std::cout << "Changed" << std::endl;
}


std::string exec(const char* cmd) {
    char buffer[128];
    std::string result;
   	FILE *fp;
   	fp = popen(cmd, "r");
	int bytes;
   	while(bytes = fread(buffer, 1, 128, fp))
   		for(int i=0;i<bytes;i++)  result += buffer[i];

   	pclose(fp);
   	return result;
}

void save_clicked()
{
	Gtk::FileChooserDialog dialog("", Gtk::FILE_CHOOSER_ACTION_OPEN);
	dialog.add_button("Open", Gtk::RESPONSE_OK);
	dialog.add_button("_Cancel", Gtk::RESPONSE_CANCEL);
	if(dialog.run() != Gtk::RESPONSE_OK) return;
	std::string filepath = dialog.get_filename();
	std::ofstream fp;
	fp.open(filepath);
	fp << exec("pdfroff -e -ms /tmp/gtroff.mom");
	fp.close();
}
void handle_mom()
{
	pbuff->set_modified(0);
    while(running)
    {
        std::this_thread::sleep_for(std::chrono::seconds(5));
        //std::cout << "Wait time over" << std::endl;
        if(!pbuff->get_modified()) continue;
        std::ofstream fp;
     	fp.open("/tmp/gtroff.mom", std::ofstream::out | std::ofstream::trunc);
     	fp << pbuff->get_text();
		fp.close();
		GError *err;
		
		std::string pdfeam = exec("pdfroff -e -ms /tmp/gtroff.mom");
		err = NULL;

     	fp.open("/tmp/gtroff.pdf", std::ofstream::out | std::ofstream::trunc);
     	fp << pdfeam;
     	fp.close();

		doc = ev_document_factory_get_document("file:///tmp/gtroff.pdf", NULL);
		if(!doc) std::cerr << err->message << std::endl;
		ev_document_model_set_document(model, doc);
		pbuff->set_modified(0);
		//std::cout << pdfeam << std::endl;
    }
}

int main(int argc, char *argv[])
{
	auto app = Gtk::Application::create(argc, argv, "me.initd.Gtroff");
	auto builder = Gtk::Builder::create();
	Gtk::Window *win;
	Gtk::TextView *tview;
	Gtk::ScrolledWindow *scrl;
	GtkWidget *view;
	Gtk::Button *cmdsave;
	ev_init();
	try
	{
		builder->add_from_file("res/main.glade");
	}
	catch(const Glib::FileError& ex)
	{
		std::cerr << "FileError: " << ex.what() << std::endl;
		return 1;
	}
	catch(const Glib::MarkupError& ex)
	{
		std::cerr << "MarkupError: " << ex.what() << std::endl;
		return 1;
	}
	catch(const Gtk::BuilderError& ex)
	{
		std::cerr << "BuilderError: " << ex.what() << std::endl;
		return 1;
	}
	builder->get_widget("frmMain", win);
	builder->get_widget("txtMain", tview);
	builder->get_widget("scrlPdf", scrl);
	builder->get_widget("cmdSave", cmdsave);
	view = ev_view_new();
	model = ev_document_model_new();
	ev_view_set_model((EvView *)view, model);
	gtk_container_add(GTK_CONTAINER(scrl->gobj()), (GtkWidget *)view);
	win->show_all_children();

	pbuff = tview->get_buffer();
	pbuff->signal_end_user_action().connect(sigc::ptr_fun(&tview_changed));
	cmdsave->signal_clicked().connect(sigc::ptr_fun(&save_clicked));
	std::thread t1(handle_mom);
	app->run(*win);
	running = 0;
	
	t1.join();

}
