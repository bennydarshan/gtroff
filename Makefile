CC = g++ -std=c++11
CFLAGS = `pkg-config --cflags gtkmm-3.0 evince-document-3.0 evince-view-3.0`
CLIBS = `pkg-config --libs gtkmm-3.0 evince-document-3.0 evince-view-3.0`

gtroff: src/main.cpp
	$(CC) -o gtroff $^ $(CFLAGS) $(CLIBS)

clean:
	@rm -f gtroff dist/*.o

run: gtroff
	./gtroff

